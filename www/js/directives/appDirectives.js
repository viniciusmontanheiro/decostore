"use strict";


angular.module("formDirectives", [])
  /**
   * @desc Máscara para moeda
   * @dependencies Jquery price format
   * @link http://jquerypriceformat.com/
   */
  .directive('coMoeda', ['$filter', function ($filter) {

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        if (!ctrl) return;

        //metodo que formata o valor quando é alterado via controller
        ctrl.$formatters.unshift(function (a) {
          elem[0].value = ctrl.$modelValue;
          elem.priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
          });
          return elem[0].value;
        });

        //Metodo que formata o valor quando é alterado via html
        ctrl.$parsers.unshift(function (viewValue) {
          elem.priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
          });
          return elem[0].value;
        });

        //Metodo que seta o valor no model
        ctrl.$parsers.push(function (valueFromInput) {
          console.log(valueFromInput);
          return parseFloat(valueFromInput.replace(/\.+/g, "").replace(/\,/, '.'));
        });
      }
    };
  }])
  /** mask
   * Ex: <input type='text' mask='9999-9999' placeholder='Telefone'>
   * 9: Qualquer numero;
   * A: Qualquer letra;
   * *: Qualquer numero e letra;
   *
   * @mask : options{
     * 	cep: 99999-999;
     * 	ddd:telefone : (99) 9999-9999;
     *  ddd: (99);
     *  telefone: 9999-9999;
     *  cpf: 999.999.999-99;
     *  cnpj: 99.999.999/9999-99;
     *
     * */
  .directive('mask', function () {
    return {
      restrict: "A",
      link: function (scope, elem, attr, ctrl) {
        if (attr.mask) {
          var mask = "";
          switch (attr.mask) {
            case 'cep':
              mask = "99999-999";
              break;
            case 'ddd:telefone':
              mask = "(99) 9999-9999";
              break;
            case 'ddd':
              mask = "(99)";
              break;
            case 'telefone':
              mask = "9999-9999";
              break;
            case 'cpf':
              mask = "999.999.999-99";
              break;
            case 'cnpj':
              mask = "99.999.999/9999-99";
              break;
            default:
              mask = attr.mask;
          }
          $(elem).mask(mask, { placeholder: attr.maskPlaceholder });
        }
      }
    };
  })

  /** mask-money
   * Ex: <input type='text' mask-money>
   *  money@params: prefix,suffix,affixesStay,thousands,decimal,precision,allowZero,allowNegative;
   *    Ex: <input mask-money="R$" money-prefix="R$" money-thousands=".">
   * */
  .directive('maskMoney', function () {
    return {
      restrict: "A",
      link: function (scope, elem, attr) {
        elem.maskMoney({
          prefix: attr.moneyPrefix ? attr.moneyPrefix : (attr.maskMoney ? attr.maskMoney : "R$ "),
          suffix: attr.moneySuffix,
          affixesStay: attr.moneyAffixesStay,
          thousands: attr.moneyThousands ? attr.moneyThousands : ".",
          decimal: attr.moneyDecimal ? attr.moneyDecimal : ",",
          precision: attr.moneyPrecision,
          allowZero: attr.moneyAllowZero,
          allowNegative: attr.moneyAllowNegative
        });
      }
    };
  })

  /**
   * @dependencies Requer um ng-model setado no elemento
   * @desc campo que estiver esse parametro aceitara somente numeros
   * */
  .directive('number', function () {
    return {
      require: '?ngModel',
      link: function (scope, element, attrs, ngModelCtrl) {
        if (!ngModelCtrl) {
          return;
        }

        ngModelCtrl.$parsers.push(function (val) {
          if (angular.isUndefined(val)) {
            var val = '';
          }
          var clean = val.replace(/[^0-9]+/g, '');
          if (val !== clean) {
            ngModelCtrl.$setViewValue(clean);
            ngModelCtrl.$render();
          }
          return clean;
        });

        element.bind('keypress', function (event) {
          if (event.keyCode === 32) {
            event.preventDefault();
          }
        });
      }
    };
  })
  /**
   * @desc máscara e válidação para coordenadas
   */
  .directive('coordenada', ['$filter', function ($filter) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {

        if (!ctrl) return;

        ctrl.$formatters.unshift(function (a) {
          elem[0].value = ctrl.$modelValue;
          elem.priceFormat({
            prefix: '',
            allowNegative: true,
            thousandsSeparator: '',
            limit: 17,
            centsLimit: 15
          });
          return elem[0].value;
        });
        //metodo que formata o valor quando é alterado via controller

        //Metodo que formata o valor quando é alterado via html
        ctrl.$parsers.unshift(function (viewValue) {
          elem.priceFormat({
            prefix: '',
            allowNegative: true,
            thousandsSeparator: '',
            limit: 17,
            centsLimit: 15
          });
          return elem[0].value;
        });

        //Metodo que seta o valor no model
        ctrl.$parsers.push(function (valueFromInput) {
          var coord = valueFromInput.toString();
          if ((coord != undefined && coord != null && coord.length > 0) && coord.search("-") === -1) {
            coord = "-" + coord;
          }
          return coord;
        });
      }
    };
  }]) .directive('exportTable', function () {
  return {
    restrict: 'C',
    link: function($scope, elm, attr) {

      $scope.$on('export-pdf', function (e, d) {
        elm.tableExport({type: 'pdf', escape: false});
      });
      $scope.$on('export-excel', function (e, d) {
        elm.tableExport({type: 'excel', escape: false});
      });
      $scope.$on('export-doc', function (e, d) {
        elm.tableExport({type: 'doc', escape: false});
      });
    }
  }
}) .directive('fabMenu', function($timeout, $ionicGesture,Util) {

  var options = {
      baseAngle: 270,
      rotationAngle: 90,
      distance: 41,
      animateInOut: 'all', // can be slide, rotate, all
    },
    buttons = [],
    buttonContainers = [],
    buttonsContainer = null,
    lastDragTime = 0,
    currentX = 0,
    currentY = 0,
    previousSpeed     = 15,

    init = function() {

      buttons = document.getElementsByClassName('fab-menu-button-item');
      buttonContainers = document.querySelectorAll('.fab-menu-items > li');
      buttonsContainer = document.getElementsByClassName('fab-menu-items');

      for (var i = 0; i < buttonContainers.length; i++) {

        var button = buttonContainers.item(i);
        var angle = (options.baseAngle + (options.rotationAngle * i));
        button.style.transform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
        button.style.WebkitTransform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
        button.setAttribute('angle', '' + angle);
      }
    },

    animateButtonsIn = function() {
      for (var i = 0; i < buttonContainers.length; i++) {

        var button = buttonContainers.item(i);
        var angle = button.getAttribute('angle');
        button.style.transform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
        button.style.WebkitTransform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
      }
    },
    animateButtonsOut = function() {
      for (var i = 0; i < buttonContainers.length; i++) {

        var button = buttonContainers.item(i);
        var angle = (options.baseAngle + (options.rotationAngle * i));
        button.setAttribute('angle', '' + angle);
        button.style.transform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
        button.style.WebkitTransform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
      }
    },

    rotateButtons = function(direction, speed) {

      // still looking for a better solution to handle the rotation speed
      // the direction will be used to define the angle calculation

      // max speed value is 25 // can change this :)
      // used previousSpeed to reduce the speed diff on each tick
      speed = (speed > 15) ? 15 : speed;
      speed = (speed + previousSpeed) / 2;
      previousSpeed = speed;

      var moveAngle = (direction * speed);

      // if first item is on top right or last item on bottom left, move no more
      if ((parseInt(buttonContainers.item(0).getAttribute('angle')) + moveAngle >= 285 && direction > 0) ||
        (parseInt(buttonContainers.item(buttonContainers.length - 1).getAttribute('angle')) + moveAngle <= 345 && direction < 0)
      ) {
        return;
      }

      for (var i = 0; i < buttonContainers.length; i++) {

        var button = buttonContainers.item(i),
          angle = parseInt(button.getAttribute('angle'));

        angle = angle + moveAngle;

        button.setAttribute('angle', '' + angle);

        button.style.transform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
        button.style.WebkitTransform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
      }
    },

    endRotateButtons = function() {

      for (var i = 0; i < buttonContainers.length; i++) {

        var button = buttonContainers.item(i),
          angle = parseInt(button.getAttribute('angle')),
          diff = angle % options.rotationAngle;
        // Round the angle to realign the elements after rotation ends
        angle = diff > options.rotationAngle / 2 ? angle + options.rotationAngle - diff : angle - diff;

        button.setAttribute('angle', '' + angle);

        button.style.transform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
        button.style.WebkitTransform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
      }
    };

  return {
    templateUrl: "templates/fab-menu.html",
    link: function(scope) {

      init();

      scope.fabMenu = {
        active: false
      };

      var menuItems = angular.element(buttonsContainer);

      $ionicGesture.on('touch', function(event) {

        console.log('drag starts', event);
        lastDragTime = 0;
        currentX = event.gesture.deltaX;
        currentY = event.gesture.deltaY;
        previousSpeed = 0;

      }, menuItems);

      $ionicGesture.on('release', function(event) {
        endRotateButtons();
      }, menuItems);

      $ionicGesture.on('drag', function(event) {

        if (event.gesture.timeStamp - lastDragTime > 100) {

          var direction = 1,
            deltaX = event.gesture.deltaX - currentX,
            deltaY = event.gesture.deltaY - currentY,
            delta = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));

          if ((deltaX <= 0 && deltaY <= 0) || (deltaX <= 0 && Math.abs(deltaX) > Math.abs(deltaY))) {
            direction = -1;
          } else if ((deltaX >= 0 && deltaY >= 0) || (deltaY <= 0 && Math.abs(deltaX) < Math.abs(deltaY))) {
            direction = 1;
          }

          rotateButtons(direction, delta);

          lastDragTime = event.gesture.timeStamp;
          currentX = event.gesture.deltaX;
          currentY = event.gesture.deltaY;
        }
      }, menuItems);

      scope.fabMenuToggle = function() {

        if (scope.fabMenu.active) { // Close Menu
          animateButtonsOut();
        } else { // Open Menu
          animateButtonsIn();
        }
        scope.fabMenu.active = !scope.fabMenu.active;
      }

    }
  }
});


