angular.module('starter.EntrarCtrl', [])
  .controller('EntrarCtrl', function ($rootScope, $scope, $stateParams, $state, $ionicModal, ApiService, ApiSpinner,IonicAlertService) {

    $scope.$on("$ionicView.beforeEnter", function () {
      $scope.loginData = {};
    });

    $scope.$on("$ionicView.loaded", function () {
      ApiSpinner.hide();
      $scope.platform = {
        isIOS : $rootScope.isIOS
      };
    });

  $scope.facebookLogin = function () {
    ApiSpinner.hide();
    facebookConnectPlugin.login(["public_profile", "email"], function (response) {

        $rootScope.user = {
          id: response.authResponse.userID,
          photoUrl: "http://graph.facebook.com/" + response.authResponse.userID + "/picture?type=large"
        };

        $state.go('cadastrar', {
          user: {
            id: response.authResponse.userID,
            photoUrl: "http://graph.facebook.com/" + response.authResponse.userID + "/picture?type=large"
          }
        });

        ApiSpinner.hide();
      },
      function loginError(error) {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Dados inválidos!')
      }
    );
  };

  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
  });

  $scope.login = function () {
    $scope.modal.show();
  };

  $scope.doLogin = function () {
    ApiSpinner.show();

    if($scope.loginData.email == ""){
      return IonicAlertService.showAlert('Atenção','Você precisa informar o e-mail!')
    }else{
      if($scope.loginData.email==""){
        IonicAlertService.showAlert('Informe sua senha!')
      }
    }

    ApiService.request({
      url: 'http://lettesapi.azurewebsites.net/api/token',
      method: 'POST',
      data: {
        grant_type: "password",
        client_secret: $scope.loginData.senha,
        client_id: $scope.loginData.email
      }
    }).success(function (data, status) {
      if (status == 200) {
        window.localStorage['access_token'] = "Bearer " + data.access_token;
        $scope.modal.hide();
        $state.go('app.mostruario');
      }
    }).error(function (err,status){
      ApiSpinner.hide();
      IonicAlertService.showAlert('Atenção','Usuário ou senha inválidos!')
    });
  };

  $scope.goToPage = function (url) {
    location.href = url;
  };

});
