
angular.module('starter.CadastroCtrl', [])
  .controller('CadastroCtrl', function ($rootScope, $scope, $stateParams, $state, ApiService, $ionicPopup, ApiMessageService, ApiSpinner,IonicAlertService) {

    $scope.$on("$ionicView.beforeEnter", function () {
      ApiSpinner.hide();

      $rootScope.user = $rootScope.user || {};

      $scope.usuario = {
        name: '',
        email: '',
        senha: ''
      };

      if ($stateParams.user) {

        facebookConnectPlugin.api("/me?fields=name,email", null, function onSuccess(response) {
          $scope.usuario.name = response.name || "";
          $scope.usuario.email = response.email || "";

          $rootScope.user.email = response.name || "";
          $rootScope.user.name = response.email || "";
        }, function onError() {
          IonicAlertService.showAlert('Atenção','Dados inválidos!');
        });
      }
    });

    $scope.$on("$ionicView.loaded", function () {
      ApiSpinner.hide();
    });

  function validarForm(usuario) {
    if (usuario.name.length == 0) {
      IonicAlertService.showAlert('Atenção','Você precisa informar o nome!');
      return false;
    } else if (usuario.email.length == 0) {
      IonicAlertService.showAlert('Atenção','Você precisa informar o e-mail!');
      return false;
    } else if (usuario.senha.length == 0) {
      IonicAlertService.showAlert('Atenção','Você precisa informar a senha!');
      return false;
    }
    return true;
  }

  $scope.cadastrar = function () {
    ApiSpinner.show();
    var usuario = angular.copy($scope.usuario);

    if (!validarForm(usuario)) {
      ApiSpinner.hide();
      return false;
    }

    $rootScope.user.email = usuario.email;
    $rootScope.user.name = usuario.name;

    ApiService.save('http://lettesapi.azurewebsites.net/api/user', {
      FullName: usuario.name,
      Email: usuario.email,
      Password: usuario.senha,
      ConfirmPassword: usuario.senha,
      ProfileImageUrl: $rootScope.user.photoUrl,
      FacebookId: $rootScope.user.id
    }).success(function (data, status) {
      ApiSpinner.hide();
      login(usuario);
    }).error(function (err) {
      ApiSpinner.hide();
      IonicAlertService.showAlert('Atenção','O e-mail informado já existe!');
    });
  };

  function login(usuario) {
    ApiSpinner.show();
    ApiService.request({
      url: 'http://lettesapi.azurewebsites.net/api/token',
      method: 'POST',
      data: {
        grant_type: "password",
        client_secret: usuario.senha,
        client_id: usuario.email
      }
    }).success(function (data, status) {
      /* Salva o token na memoria */
      window.localStorage['access_token'] = "Bearer " + data.access_token;
      $state.go('app.mostruario');
      ApiSpinner.hide();
    }).error(function(){
      ApiSpinner.hide();
      IonicAlertService.showAlert('Atenção','Usuário ou senha inválidos!');
    });
  }

});
