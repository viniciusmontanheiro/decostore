angular.module('starter.AlterarPlaylistCtrl', [])
  .controller('AlterarPlaylistCtrl', function
    AlterarPlaylistController($scope, $state, ApiService, ApiSpinner, $stateParams, IonicAlertService, $rootScope, $ionicHistory)
  {

    $scope.$on("$ionicView.beforeEnter", function () {
      $scope.listaItens = $stateParams.listaItens;

      $scope.lista = {
        nome: ''
      };

      $scope.produto = {
        buscar: ''
      };

      $scope.lista.nome = $scope.listaItens.Name;
      $scope.voltar = $ionicHistory.goBack;
    });

    $scope.$on("$ionicView.loaded", function () {
      ApiSpinner.hide();
    });

    $scope.$on('$ionicView.beforeLeave', function () {
      $scope.minhasListas = [];
      $scope.lista = {};
      $scope.listaItens = [];
    });

    $scope.buscarProduto = function (){
      $rootScope.descricao = $scope.produto.buscar || "";
      $state.go('app.mostruario');
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.editarLista = function () {
      ApiSpinner.show();
      if ($scope.lista.nome == "") {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção', 'Você precisa informar o nome!');
        return false;
      }

      var lista = {
        Name: $scope.lista.nome
      };

      ApiService.updateAndAuth("http://lettesapi.azurewebsites.net/api/List/" + $scope.listaItens.Id, lista)
        .success(function () {
          $state.go('app.playlist');
        }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Não foi possível alterar!', 'Verifique sua conexão com a internet.');
      });
    };
  });
