angular.module('starter.CadastrarPlaylistCtrl', [])
  .controller('CadastrarPlaylistCtrl',
    function CadastrarPlaylistController($scope, $state, ApiService, ApiSpinner, IonicAlertService, $stateParams, $rootScope, $ionicHistory) {

      $scope.$on("$ionicView.beforeEnter", function () {
        $scope.lista = {
          nome: ''
        };
        $scope.selecionado = $stateParams.produto || {};
        $scope.produto = {
          buscar: ''
        };
        $scope.voltar = $ionicHistory.goBack;
      });

      $scope.$on("$ionicView.loaded", function () {
        ApiSpinner.hide();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $scope.lista.nome = '';

      });

      $scope.buscarProduto = function (){
        $rootScope.descricao = $scope.produto.buscar || "";
        $state.go('app.mostruario');
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.salvarPlaylist = function () {
        ApiSpinner.show();
        if ($scope.lista.nome == "") {
          ApiSpinner.hide();
          IonicAlertService.showAlert('Atenção', 'Você precisa informar o nome!');
          return false;
        }

        var nome = {
          Name: $scope.lista.nome
        };

        ApiService.saveAndAuth("http://lettesapi.azurewebsites.net/api/List", nome)
          .success(function (retorno) {

            if ($scope.selecionado.Id) {
              ApiService.saveAndAuth('http://lettesapi.azurewebsites.net/api/list/item', {
                ListId: retorno.Id,
                ProductId: $scope.selecionado.Id
              }).success(function () {
                ApiSpinner.hide();
              }).error(function () {
                ApiSpinner.hide();
                IonicAlertService.showAlert('Não foi possível salvar!', 'Verifique sua conexão com a internet.');
              });
            }
            $state.go('app.mostruario');

          }).error(function () {
          ApiSpinner.hide();
          IonicAlertService.showAlert('Não foi possível salvar!', 'Verifique sua conexão com a internet.');
        });
      };

    });
