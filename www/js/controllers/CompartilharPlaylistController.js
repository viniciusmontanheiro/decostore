angular.module('starter.CompartilharPlaylistCtrl', [])
  .controller('CompartilharPlaylistCtrl',
    function CompartilharPlaylistController($scope, $rootScope, $state, ApiService, ApiSpinner, $stateParams, IonicAlertService) {

      $scope.$on("$ionicView.beforeEnter", function () {
        $scope.listaItens = $stateParams.listaItens;
        $scope.lista = {
          nome: $scope.listaItens.Name
        };
        $scope.produto = {
          buscar: ''
        };
        $scope.voltar = $ionicHistory.goBack;
      });

      $scope.$on("$ionicView.loaded", function () {
        ApiSpinner.hide();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $scope.listaItens = [];
        $scope.lista.nome = "";
      });

      $scope.buscarProduto = function () {
        $rootScope.descricao = $scope.produto.buscar || "";
        $state.go('app.mostruario');
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.compartilharLista = function () {
        ApiSpinner.show();
        if ($scope.lista.email == "") {
          ApiSpinner.hide();
          IonicAlertService.showAlert('Atenção', 'Você precisa informar o e-mail!');
          return false;
        }

        var data = {
          UserName: $scope.lista.email,
          ListId: $scope.listaItens.Id
        };

        ApiService.saveAndAuth("http://lettesapi.azurewebsites.net/api/list/user", data)
          .success(function () {
            $state.go('app.playlist');
          }).error(function () {
          ApiSpinner.hide();
          IonicAlertService.showAlert('Não foi possível compartilhar!', 'Verifique a sua conexão com a internet.');
        });
      };

    });
