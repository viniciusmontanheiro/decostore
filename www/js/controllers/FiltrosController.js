angular.module('starter.FiltrosCtrl', [])
  .controller('FiltrosCtrl', function ($scope, $state, ApiService, ApiSpinner, $rootScope,IonicAlertService,$ionicHistory,ApiDecostore) {

    $scope.$on("$ionicView.loaded", function () {
      ApiSpinner.hide();
      $scope.filtros = [];
      $scope.materials = [];
      $scope.styles = [];
      $scope.grupo = [];
      $scope.color = {};
      $scope.style = {
        Description:""
      };
      $scope.material = {
        Description : ""
      };
      $rootScope.filtrados = [];
      $scope.listarFiltros();
      $scope.produto = {
        buscar: '',
        liked : false,
        price : ""
      };
      $scope.voltar = $ionicHistory.goBack;

        $('.price').click(function (e) {
          $('.price').removeClass('accent');
          $(this).addClass('accent');
          //$scope.price = $(e.currentTarget).attr("value");
          e.preventDefault();
        });
    });

    $scope.$on('$ionicView.beforeLeave', function(){
      $scope.limparFiltros();
    });

    $scope.buscarProduto = function (){
      $rootScope.descricao = $scope.produto.buscar || "";
      $state.go('app.mostruario');
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.listarFiltros = function () {
      ApiSpinner.show();
      ApiService.request({
        url: 'http://lettesapi.azurewebsites.net/api/filter',
        method: 'GET',
        authorization: window.localStorage['access_token']
      }).success(function (filtros) {
        if (filtros.Materials && filtros.Materials.length >= 1) {
          $scope.filtros = filtros;
          $scope.materials = filtros.Materials;
          $scope.styles = filtros.Styles;
          $scope.grupo = filtros.ColorGroups;
        }
        ApiSpinner.hide();
      }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a intetnet!');
      });

    };

    $scope.limparFiltros = function () {
      $rootScope.filtrados = [];
      $scope.produto.price = "";
      $scope.produto.liked = false;
      $scope.color = {};
      $scope.style = {};
      $scope.material = {};
      $('.group-style').animate({boxShadow: '0 2px 3px rgba(0, 0, 0, 0.3) !important'},100);
      $('.group-material').animate({boxShadow: '0 2px 3px rgba(0, 0, 0, 0.3) !important'},100);
      $('.group-color').animate({boxShadow: '0 2px 3px rgba(0, 0, 0, 0.3) !important'},100);
      $('.material-image').removeClass('checked');
      $('.style-image').removeClass('checked');
      $('.style-color').removeClass('checked');
      $('.price').removeClass('accent');
      $("input[type=checkbox]").attr("checked",false);
    };

    $scope.onMaterialClick = function (e,material) {
      $scope.material = material;
      $('.group-material').animate({boxShadow: '0 2px 3px rgba(0, 0, 0, 0.3) !important'},100);
      $(e.currentTarget).animate({boxShadow: '0 3px 8px rgba(0, 0, 0, 0.3) !important'},100);
    };

    $scope.onStyleClick = function (e,style) {
      $scope.style = style;
      $('.group-style').animate({boxShadow: '0 2px 3px rgba(0, 0, 0, 0.3) !important'},100);
      $(e.currentTarget).animate({boxShadow: '0 3px 8px rgba(0, 0, 0, 0.3) !important'},100);
    };

    $scope.onColorClick = function (e,color) {
      $scope.color = color;
      $('.group-color').animate({boxShadow: '0 2px 3px rgba(0, 0, 0, 0.3) !important'},100);
      $(e.currentTarget).animate({boxShadow: '0 3px 8px rgba(0, 0, 0, 0.3) !important'},100);
    };

    $scope.filtrarProdutos = function () {
      ApiSpinner.show();

      var query = {};

      if($scope.color.Id){
        query.IdFilterColorGroup = $scope.color.Id;
      }

      if($scope.material.Description){
        query.Material = $scope.material.Description;
      }

      if($scope.style.Description){
        query.Style = $scope.style.Description;
      }

      if($scope.produto.price){
        query.OrderBy = $scope.produto.price;
      }

      if($scope.produto.liked){
        query.Liked = $scope.produto.liked;
      }

      ApiService.queryAndAuth("http://lettesapi.azurewebsites.net/api/catalog",query)
        .success(function (filtrados) {
          $rootScope.filtrados = filtrados;
          $state.go('app.mostruario');
        }).error(function (err) {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','verifique sua conexão com a internet!');
      });

    };

  });
