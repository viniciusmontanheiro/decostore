angular.module('starter.MostruarioCtrl', [])
  .controller('MostruarioCtrl', function ($rootScope,$scope, $state, ApiService, $stateParams, $ionicPopup, $ionicLoading, ApiSpinner,$filter,IonicAlertService,ApiDecostore) {

  $scope.$on("$ionicView.loaded", function () {
    $scope.listaProdutos = [];
    $scope.listaProdutosAux = [];
    $scope.showList = false;
    $scope.selecionado = {};
    $rootScope.filtrados = [];
    $rootScope.relacionados = [];
    $scope.produto = {
      buscar : '',
      success : false
    };

    $scope.listaPlaylist = [];

  });

    $scope.$on('$ionicView.beforeEnter', function(){
      $scope.produto = $stateParams.produto || {};

      if ($rootScope.filtrados.length >= 1) {
        $scope.listaProdutos = $rootScope.filtrados;
      } else {
        if($scope.listaProdutos.length <= 0){
          $scope.buscarProdutos("http://lettesapi.azurewebsites.net/api/Catalog");
        }
      }

      if($rootScope.descricao != ""){
        $scope.produto.buscar = $rootScope.descricao;
      }

      ApiSpinner.hide();
    });

  $scope.$on('$ionicView.beforerLeave', function(){
    $scope.selecionado = {};
    $rootScope.filtrados = [];
    //$scope.listaProdutos = [];
    $scope.listaProdutosAux = [];
  });

  $scope.buscar = function(descricao){
    var resultado  =  $filter('filter')($scope.listaProdutos, {Name : descricao});

    if(descricao == ""){
      $scope.listaProdutos = $scope.listaProdutosAux;
    }else{
      $scope.listaProdutos = resultado;
    }
  };

    $scope.onClickDetalharProduto = function (produto) {
    ApiSpinner.show();
    $state.go('app.detalharMostruario', {produto: produto});
  };

  $scope.onRefresh = function () {
    $scope.produto.buscar = "";
    $scope.buscarProdutos('http://lettesapi.azurewebsites.net/api/Catalog');
  };

  $scope.buscarProdutos = function (url) {
    ApiSpinner.show();

    var t1 = new Date();
    var t2 = null;
    var seconds = null;

    ApiService.request({
      url: url,
      method: 'GET',
      authorization: window.localStorage['access_token']
    }).success(function (produtos) {

      t2 = new Date();
      seconds = (t2.getTime() - t1.getTime()) / 1000;
      seconds = Math.abs(seconds);

      if (produtos && produtos.length >= 1) {
        $scope.listaProdutos = produtos;
        $scope.listaProdutosAux = produtos;
        $rootScope.relacionados = produtos;
        if($scope.produto.success){
          $scope.buscar($scope.produto.buscar);
        }
        ApiSpinner.hide();
        $scope.$broadcast('scroll.refreshComplete');
      }
      console.log(seconds);

    }).error(function (err,status) {
      ApiSpinner.hide();
      $scope.$broadcast('scroll.refreshComplete');
      if(status == 401){
        return IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet!',function(){
            $state.go('entrar');
        });
      }else{
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet!');
      }
    });

  };

  $scope.curtir = function(produto){
    var index = $scope.listaProdutos.indexOf(produto);
    $scope.listaProdutos[index].Liked = ApiDecostore.curtir(produto);
  };


  $scope.adicionarProduto = function (lista) {
    ApiDecostore.adicionarProduto(lista,$scope);
  };

    $scope.popupPlaylist = function(produto){
      $scope.selecionado = produto;
      ApiSpinner.show();

      ApiService.request({
        url: 'http://lettesapi.azurewebsites.net/api/list',
        method: 'GET',
        authorization: window.localStorage['access_token']
      }).success(function (listas) {
        ApiSpinner.hide();
        $scope.listaPlaylist = listas;
        $scope.popupList = $ionicPopup.show({
          templateUrl: 'popupAddToList.html',
          'scope':$scope,
          cssClass: 'add-to-list-popup'
        });

      }).error(function (err) {
        $scope.popupList.close();
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção', 'Verifique sua conexão com a internet!');
      });
    };

   $scope.cadastrar = function(){
     ApiDecostore.cadastrarPlaylist($scope);
   };

});
