angular.module('starter.AppCtrl', [])
  .controller('AppCtrl', function ($rootScope, $scope, $ionicModal, $stateParams, $timeout, $state,
                                   ApiService,IonicAlertService,ApiSpinner,$ionicPopover,Util,$cordovaCamera,$ionicPopup)
  {

    $scope.$on("$ionicView.loaded", function () {
      $.material.init();
      $.material.ripples();
      $.material.input();
      $.material.checkbox();
      $.material.radio();

      $('#menu .item').click(function (e) {
        $('#menu .item').removeClass('ativo');
        $(this).addClass('ativo');
        e.preventDefault();
      });

      $('.fix-border').removeClass('item-complex');

      ApiSpinner.hide();
      $rootScope.user = {};
      $scope.popover = {};

      $ionicPopover.fromTemplateUrl('templates/popover.html', {
        scope: $scope
      }).then(function(popover) {
        $scope.popover = popover;
      });

      ApiService.request({
        url: 'http://lettesapi.azurewebsites.net/api/user',
        method: 'GET',
        authorization: window.localStorage['access_token']
      }).success(function (data) {
        ApiSpinner.hide();
        $rootScope.user = data;
        $rootScope.user.ProfileImageUrl = $scope.user.ProfileImageUrl || 'img/profile.png';
      }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet!');
      });
    });

    $scope.logout = function () {
      ApiSpinner.show();
      window.localStorage.clear();
      delete window.localStorage['access_token'];
      $state.go('entrar');
    };

    $scope.changeLocation = function(url){
      window.open(
        url,
        '_blank'
      );
    };

    $scope.stateGo = function(url){
      $state.go(url);
    };

    $scope.abrirPopupCamera = function(){
      $scope.popupCamera = $ionicPopup.show({
        templateUrl: 'popupCamera.html',
        'scope':$scope,
        cssClass: 'add-to-list-popup'
      });
    };

    $scope.upload = function(data){
      ApiService.request({
        url: 'http://lettesapi.azurewebsites.net/api/user/profileimage',
        method: 'POST',
        data : data,
        headers: {'Content-Type': 'image/jpeg"'},
        authorization: window.localStorage['access_token']
      }).success(function () {
        $scope.popupCamera.close();
        ApiSpinner.hide();
      }).error(function (err,status,headers) {
        $scope.popupCamera.close();
        ApiSpinner.hide();
      });
    };

    $scope.takePhoto = function () {
      $scope.popupCamera.close();
      var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.imgURI = "data:image/jpeg;base64," + imageData;

        var data = {
          file : imageData
        };

        $scope.upload(imageData);

      }, function (err) {
        console.log(err);
        $scope.popupCamera.close();
        ApiSpinner.hide();
      });
    };

    $scope.choosePhoto = function () {
      $scope.popupCamera.close();
      var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.imgURI = "data:image/jpeg;base64," + imageData;

        var data = {
          file : imageData
        };

        $scope.upload($scope.imgURI);

      }, function (err) {
        console.log(err);
        $scope.popupCamera.close();
        ApiSpinner.hide();
      });
    };

  });







