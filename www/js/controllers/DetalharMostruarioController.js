angular.module('starter.DetalharMostruarioCtrl', [])
  .controller('DetalharMostruarioCtrl',
    function
      DetalharMostruarioController($scope, $state, ApiService, ApiSpinner, $stateParams, $ionicScrollDelegate,
                                   $ionicActionSheet, $ionicHistory, $rootScope, $ionicPopup, ApiDecostore, Util,IonicAlertService,$ionicListDelegate,$cordovaSocialSharing) {

      $scope.$on("$ionicView.beforeEnter", function () {
        $scope.produto = $stateParams.produto;
        $scope.voltar = $ionicHistory.goBack;
        $scope.relacionados = $rootScope.relacionados;
        $scope.listaPlaylist = [];

        $scope.descricao = {
          buscar: ''
        };
      });

      $scope.$on("$ionicView.loaded", function () {
        ApiSpinner.hide();

        $scope.isIOS = ionic.Platform.isIOS();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $scope.produto = {};
        $scope.relacionados = [];
        $stateParams.produto = {};
        $scope.listaPlaylist = [];
      });

      $scope.onClickDetalharProduto = function (produto) {
        $scope.produto = produto;
        $ionicScrollDelegate.scrollTop();
      };

      $scope.buscarProduto = function (){
        $rootScope.descricao = $scope.descricao.buscar || "";
        $state.go('app.mostruario');
        $scope.$broadcast('scroll.refreshComplete');
      };

      $scope.showOppaActionSheet = function () {

        $ionicActionSheet.show({
          titleText: '',
          buttons: [
            {text: '<i class="fa fa-link"></i>&nbsp; Ir para o site da loja'},
            {text: '<i class="fa fa-envelope"></i>&nbsp; Enviar link por e-mail'}
          ],
          cancelText: '<i class="fa fa-close"></i>&nbsp; Cancelar',

          cancel: function () {
            console.log('CANCELLED');
          },
          buttonClicked: function (index) {
            if (index <= 0) {

              window.open(
                $scope.produto.Link,
                '_blank'
              );
            } else {
              if (index == 1) {

                var params = {
                   ProductId: $scope.produto.Id
                };
                ApiSpinner.show();
                ApiService.saveAndAuth("http://lettesapi.azurewebsites.net/api/product/shareproductbyemail", params)
                  .success(function (retorno) {
                      ApiSpinner.hide();
                    IonicAlertService.showAlert('Alerta de Confirmação', 'o link do produto foi enviado!');
                  }).error(function () {
                  ApiSpinner.hide();
                  IonicAlertService.showAlert('Não foi possível enviar!', 'Verifique sua conexão com a internet.');
                });
                return true;
              }else {
                return true;
              }

            }
            return true;
          }
        });
      };

      $scope.curtir = function () {
        $scope.produto.Liked = ApiDecostore.curtir($scope.produto);
      };

      $scope.adicionarProduto = function (lista) {
        ApiDecostore.adicionarProduto(lista, $scope);
      };

      $scope.popupPlaylist = function () {
        $scope.selecionado = $scope.produto;
        ApiSpinner.show();

        ApiService.request({
          url: 'http://lettesapi.azurewebsites.net/api/list',
          method: 'GET',
          authorization: window.localStorage['access_token']
        }).success(function (listas) {
          ApiSpinner.hide();
          $scope.listaPlaylist = listas;
          $scope.popupList = $ionicPopup.show({
            templateUrl: 'popupAddToList.html',
            'scope': $scope,
            cssClass: 'add-to-list-popup'
          });

        }).error(function () {
          ApiSpinner.hide();
          $scope.popupList.close();
          IonicAlertService.showAlert('Atenção', 'Verifique sua conexão com a internet!');
        });
      };

      $scope.cadastrar = function () {
        $scope.popupList.close();
        ApiDecostore.cadastrarPlaylist($scope);
      };

      $scope.changeLocation = function (url) {
        Util.fn.changeLocation(url, $scope, true);
      };

      $scope.shareWhatsApp = function() {
        $ionicListDelegate.closeOptionButtons();
        $cordovaSocialSharing
          .shareViaWhatsApp($scope.produto.Name, $scope.produto.Image, $scope.produto.Link)
          .then(function(result) {
            // Success!
          }, function(err) {

          });
      };

      $scope.shareFacebook= function() {
        $ionicListDelegate.closeOptionButtons();
        $cordovaSocialSharing
          .shareViaFacebook($scope.produto.Name, $scope.produto.Image, $scope.produto.Link)
          .then(function(result) {
            // Success!
          }, function(err) {

          });
      };





    });
