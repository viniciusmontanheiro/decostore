angular.module('starter.PlaylistCtrl', [])
  .controller('PlaylistCtrl',
    function PlaylistCtrl($scope, $state, ApiService, ApiSpinner, $stateParams,$ionicActionSheet,$rootScope,IonicAlertService,$ionicHistory) {

    $scope.$on("$ionicView.loaded", function () {
      ApiSpinner.hide();
      $scope.lista = {
        nome : '',
        email: ''
      };
      $scope.minhasListas = [];
      $scope.listaItens = [];
      $scope.playlist = {};
      $scope.produto = {
        buscar : ''
      };
    });

      $scope.$on("$ionicView.beforeEnter", function () {
        $scope.iniciarPlaylist();
      });

    $scope.$on('$ionicView.beforeLeave', function(){
      $scope.nome = "";
      $scope.lista.nome = "";
      $scope.produto.buscar = "";
    });

      $scope.buscarProduto = function (){
        $rootScope.descricao = $scope.produto.buscar || "";

        if($rootScope.descricao == ""){
            $scope.buscarListas();
        }else{
          $state.go('app.mostruario');
          $scope.$broadcast('scroll.refreshComplete');
        }

      };

    $scope.iniciarPlaylist = function (){
      $scope.listaItens = $stateParams.listaItens || {};
      $scope.minhasListas = [];
      $scope.lista.nome = $scope.listaItens ? $scope.listaItens.Name : '';
      $scope.buscarListas();
    };

    $scope.buscarListas = function () {
      ApiSpinner.show();

      ApiService.request({
        url: 'http://lettesapi.azurewebsites.net/api/list',
        method: 'GET',
        authorization: window.localStorage['access_token']
      }).success(function (listas) {
        $scope.minhasListas = listas;
        ApiSpinner.hide();
      }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet!');
      });
    };

    $scope.editarLista = function () {
      ApiSpinner.show();
      if ($scope.lista.nome == ""){
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Você precisa informar o nome!');
        return false;
      }

      var lista = {
        Name: $scope.lista.nome
      };

      ApiService.updateAndAuth("http://lettesapi.azurewebsites.net/api/List/" + $scope.listaItens.Id,lista)
        .success(function () {
        $state.go('app.playlist');
      }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Não foi possível alterar!','Verifique sua conexão com a internet.');
      });
    };

    $scope.showPLaylistActionSheet = function(item) {
      $scope.playlist = item;

      if($scope.playlist.Id == 0) return false;

      $ionicActionSheet.show({
        titleText: '',
        buttons: [
          { text: '<i class="ion-person-add fa-15x"></i>&nbsp; Compartilhar' },
          { text: '<i class="ion-edit fa-15x"></i>&nbsp; Editar' }
        ],
        destructiveText: '<i class="ion-ios-trash-outline fa-15x"></i>&nbsp; Excluir',
        cancelText: '<i class="ion-close fa-15x"></i>&nbsp; Cancelar',

        cancel: function() {
          console.log('CANCELLED');
        },
        buttonClicked: function(index) {
          $scope.listaItens = $scope.playlist;
          if(index <= 0){
            $scope.onClickCompartilhar();
          }else{
            $scope.onClickAlterarPLaylist();
          }
          return true;
        },
        destructiveButtonClicked: function() {
          $scope.excluirLista($scope.playlist);
          return true;
        }
      });
    };

    $scope.onClickAlterarPLaylist = function () {
      ApiSpinner.show();
      $state.go('app.alterarPlaylist',{listaItens: $scope.listaItens});
    };

    $scope.excluirLista = function (playlist) {
      ApiSpinner.show();
      ApiService.remove('http://lettesapi.azurewebsites.net/api/list/', playlist.Id)
        .success(function () {
          ApiSpinner.hide();
          $scope.buscarListas();
        }).error(function () {
          ApiSpinner.hide();
          IonicAlertService.showAlert('Não foi possível excluir!','Verifique sua conexão com a internet.');
      });
    };

    $scope.onClickListarItens = function (index) {
      ApiSpinner.show();
      var listaItens = $scope.minhasListas[index];

      $state.go('app.detalharPlaylist',{listaItens: listaItens });
    };

    $scope.onClickDetalharProduto = function (produto) {
      ApiSpinner.show();
      $state.go('app.detalharMostruario', {produto: produto});
    };

    $scope.onClickCompartilhar = function () {
      ApiSpinner.show();
      $state.go('app.compartilharPlaylist',{listaItens: $scope.listaItens});
    };

    $scope.cadastrar = function () {
      ApiSpinner.show();
      $state.go('app.cadastrarPlaylist');
    };

  });

