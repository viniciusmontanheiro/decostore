angular.module('starter.DetalharPlaylistCtrl', []).controller('DetalharPlaylistCtrl',
  function DetalharPlaylistCtrl($rootScope,$scope, $state, ApiService, $stateParams, $ionicPopup, $ionicLoading,
            ApiSpinner,$ionicActionSheet,IonicAlertService,$ionicHistory,ApiDecostore) {

  $scope.$on("$ionicView.beforeEnter", function () {
      $scope.listaItens = $stateParams.listaItens;
      $scope.meusProdutos = $scope.listaItens.Products;
      $scope.produto = {
        buscar: ''
      };
      $scope.voltar = $ionicHistory.goBack;
      ApiSpinner.hide();
  });

  $scope.$on("$ionicView.loaded", function () {
    ApiSpinner.hide();
  });

  $scope.$on('$ionicView.beforeLeave', function(){
    $scope.listaItens = [];
  });

    $scope.buscarProduto = function (){
      $rootScope.descricao = $scope.produto.buscar || "";
      $state.go('app.mostruario');
      $scope.$broadcast('scroll.refreshComplete');
    };

  $scope.showPLaylistItemActionsheet = function(index) {
    var produto = $scope.meusProdutos[index];

    if($scope.listaItens.Id == 0) return false;

    $ionicActionSheet.show({
      titleText: '',
      buttons: [
        { text: produto.Liked ? '<i class="fa-15x ion-heart-broken"></i>&nbsp; Descurtir' : '<i class="fa-15x ion-heart"></i>&nbsp; Curtir'
        }],
      destructiveText: '<i class="ion-ios-trash-outline fa-15x"></i>&nbsp; Excluir',
      cancelText: '<i class="ion-close fa-15x"></i>&nbsp; Cancelar',

      cancel: function() {
        console.log('CANCELLED');
      },
      buttonClicked: function() {
        $scope.curtir(produto);
        return true;
      },
      destructiveButtonClicked: function() {
        $scope.excluirPlaylistItem(produto);
        return true;
      }
    });
  };

    $scope.curtir = function(produto){
      var index = $scope.meusProdutos.indexOf(produto);
      $scope.meusProdutos[index].Liked = ApiDecostore.curtir(produto);
    };

  $scope.onClickAlterarPLaylist = function () {
    ApiSpinner.show();
    $state.go('app.alterarPlaylist',{listaItens: $scope.listaItens});
  };

  $scope.onClickDetalharProduto = function (produto) {
    ApiSpinner.show();
    $state.go('app.detalharMostruario', {produto: produto});
  };

});
