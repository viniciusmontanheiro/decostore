angular.module('starter', ['ionic',
  'ngCordova',
  'ngAnimate',
  'starter.AppCtrl',
  'starter.SplashCtrl',
  'starter.MostruarioCtrl',
  'starter.FiltrosCtrl',
  'starter.CadastroCtrl',
  'starter.EntrarCtrl',
  'starter.PlaylistCtrl',
  'starter.DetalharPlaylistCtrl',
  'starter.AlterarPlaylistCtrl',
  'starter.DetalharMostruarioCtrl',
  'starter.CompartilharPlaylistCtrl',
  'starter.CadastrarPlaylistCtrl',
  'appServices',
  'formDirectives']).constant('_', window._)

.run(function($ionicPlatform,$ionicHistory,$rootScope) {

  $rootScope.isIOS = ionic.Platform.isIOS();

  $rootScope._ = window._;

  $ionicPlatform.ready(function() {

    $ionicPlatform.onHardwareBackButton(function(){
        if($ionicHistory.viewHistory().backView.stateName == "app.mostruario"){
          $('#menu .item').removeClass('ativo');
          $($('.menu .item')[0]).addClass('ativo');
        }else{
          if($ionicHistory.viewHistory().backView.stateName == "app.playlist"){
            $('#menu .item').removeClass('ativo');
            $($('.menu .item')[1]).addClass('ativo');
          }
        }
    });

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$rootScopeProvider) {
  $ionicConfigProvider.backButton.text('').previousTitleText(false);
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.views.transition('platform');

  $rootScopeProvider.digestTtl(40);

  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('splash', {
      url: '/splash',
      templateUrl: 'templates/splash.html',
      controller: 'SplashCtrl'
    })

    .state('entrar', {
      url: '/entrar',
      templateUrl: 'templates/entrar.html',
      controller: 'EntrarCtrl'
    })

    .state('cadastrar', {
      url: '/cadastrar',
      params: {
        user: null
      },
      templateUrl: 'templates/cadastrar.html',
      controller: 'CadastroCtrl'
    })

    .state('app.filtros', {
      url: '/filtros',
      views: {
        'menuContent': {
          templateUrl: 'templates/filtros.html',
          controller:'FiltrosCtrl'
        }
      }
    })
    .state('app.mostruario', {
      url: '/mostruario',
      views: {
        'menuContent': {
          templateUrl: 'templates/mostruario.html',
          controller: 'MostruarioCtrl'
        }
      }
    })
    .state('app.detalharMostruario', {
      url: '/detalharMostruario',
      params: {
        produto: null,
        relacionado : null
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/detalhar-mostruario.html',
          controller : 'DetalharMostruarioCtrl'
        }
      }
    })
    .state('app.detalharPlaylist', {
      url: '/detalharPlaylist',
      params: {
        listaItens: []
      },
      views: {
        'menuContent': {
          templateUrl: 'templates/detalhar-playlist.html',
          controller : 'DetalharPlaylistCtrl'
        }
      }
    })
    .state('app.playlist', {
    url: '/playlist',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller:'PlaylistCtrl'
      }
    }
  }) .state('app.alterarPlaylist', {
    url: '/alterarPlaylist',
    params: {
      listaItens: []
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/alterar-playlist.html',
        controller : 'AlterarPlaylistCtrl'
      }
    }
  }) .state('app.cadastrarPlaylist', {
    url: '/cadastrarPlaylist',
    params: {
      produto: {}
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/cadastrar-playlist.html',
        controller:'CadastrarPlaylistCtrl'
      }
    }
  }).state('app.compartilharPlaylist', {
    url: '/compartilharPlaylist',
    params: {
      listaItens: []
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/compartilhar-playlist.html',
        controller : 'CompartilharPlaylistCtrl'
      }
    }
  });

  if(window.localStorage['access_token'] != null
    && window.localStorage['access_token'] != ""
    && window.localStorage['access_token'] != undefined) {
    $urlRouterProvider.otherwise('/app/mostruario');
  } else {
    $urlRouterProvider.otherwise('/splash');
  }
});
