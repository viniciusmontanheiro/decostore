"use strict";

/**
 *  Service para definição de campos multivalorados e concatenação de campos com labelFunction.
 */
angular.module("appServices", [])
  .service('Util', function ($location) {
    var methods = {};
    methods.formatarCoordenadas = formatarCoordenadas;
    methods.adicionaMenos = adicionaMenos;
    methods.isEmpty = isEmpty;
    methods.getEventPosition = getEventPosition;
    methods.changeLocation = changeLocation;

    return {
      fn: methods
    };

    /**
     * @desc Formata coordenadas SEM máscaras
     * @params String coordenada
     */
    function formatarCoordenadas(cord) {
      if (cord != undefined && cord != null && cord.length > 0) {
        cord = cord.replace(/[a-zA-Z.,]+/, "");
        cord = cord.replace(/^([0-9]{2})([0-9]+)$/, "-$1.$2");
      }
      return cord;
    }
    /**
     * @desc Verifica se possui sinal negativo na coordenada, se não, ele adiciona.
     * @params String coordenada
     */
    function adicionaMenos(str) {
      var cord = str.toString();
      if ((cord != undefined && cord != null && cord.length > 0) && cord.search("-") === -1) {
        cord = "-" + cord;
      }
      return cord;
    }
    /**
     * @desc Valida campos vazios
     * @param objeto,array
     * @returns {boolean}
     */
    function isEmpty(obj, arr) {
      if (arr != null && arr != undefined && obj != null) {
        for (var i = 0; i < arr.length; i++) {
          if (obj.hasOwnProperty(arr[i])) {
            if (obj[arr[i]] == "" || obj[arr[i]] == undefined || obj[arr[i]].length == 0) {
              return true;
            }
          } else {
            return true;
          }
        }
      }
      return false;
    }
    function getPosition(element) {
      var xPosition = 0;
      var yPosition = 0;

      while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
      }
      return { x: xPosition, y: yPosition };
    }

    function getEventPosition(event){
      var canvasPosition = getPosition(event.gesture.touches[0].target);

      var tap = { x:0, y:0 };
      if(event.gesture.touches.length>0){
        var tt = event.gesture.touches[0];
        tap.x = tt.clientX || tt.pageX || tt.screenX ||0;
        tap.y = tt.clientY || tt.pageY || tt.screenY ||0;
      }
      tap.x = tap.x - canvasPosition.x;
      tap.y = tap.y - canvasPosition.y;

      return {top: tap.y, left: tap.x};
    }


    function changeLocation(url ,$scope, forceReload) {
      $scope = $scope || angular.element(document).scope();
      if(forceReload || $scope.$$phase) {
        window.location = url;
      }
      else {
        //only use this if you want to replace the history stack
        //$location.path(url).replace();

        //this this if you want to change the URL and add it to the history stack
        $location.path(url);
        $scope.$apply();
      }
    }
  }).service('DialogService', ['$mdDialog', function ($mdDialog) {

  this.confirm = function (event, url, model) {
    var current = angular.copy($mdDialog);

    return current.show({
      parent: angular.element(document.body),
      targetEvent: event,
      templateUrl: url,
      clickOutsideToClose: true,

      animate: 'full-screen-dialog',
      locals: {
        model: model
      },
      controller: function DialogController($scope, $mdDialog) {
        $scope.model = model;
        $scope.cancel = function () {
          $mdDialog.cancel();
        };
        $scope.confirm = function () {
          $mdDialog.hide($scope.model);
        };
      }
    });
  };

}]).service('MessageService', ['toaster', function (toaster) {

  this.addError = function (message) {
    return toaster.pop("error", message);
  };

  this.addInfo = function (message) {
    return toaster.pop("info", message);
  };

  this.addSuccess = function (message) {
    return toaster.pop("success", message);
  };

}]).service('SocketService', ['$http', function ($http) {
  var client;
  this.connect = function connect() {
    client = io.connect('https://localhost:3000');
    return client;
  };

  this.disconnect = function disconnect() {
    if (client) {
      client.disconnect();
    }
  };
}])

.service('ApiMessageService', function () {

  this.getErrorMessage = function getErrorMessage(model) {
    var str = "";
    for(var i in model){
      str += model[i][0] + "<br/>"
    }
    return str;
  };

}).service('ApiSpinner', function ($ionicLoading) {

    var self = this;

    self.show = function(template) {
      $ionicLoading.show({
        template:  template || ' <ion-spinner icon="android"></ion-spinner>'
      });
    };
    self.hide = function(){
      $ionicLoading.hide();
    };

    return self;

  })
  .service('ApiService', ['$http', function ($http) {
  var self = this;

  self.query = function (url) {
    return $http.get(url);
  };

  self.findBy = function (url, id) {
    return $http.get(url, id);
  };

  self.save = function (url, obj) {
    return $http.post(url, obj);
  };

  self.update = function (url, obj) {
    return $http.put(url, obj);
  };

  self.remove = function (url, id) {
    return $http.delete(url + id,{
      headers: {'Content-Type': 'application/json','Authorization': window.localStorage['access_token']}
    });
  };

  self.request = function(request){
    request.method = request.method || 'GET';


    if(!request.headers){
      request.transformRequest = function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      };

      request.headers = {
        'Content-Type' : 'application/x-www-form-urlencoded'
      };
    }

    if(request.authorization)
      request.headers['Authorization'] = request.authorization;

    var obj = {
      url: request.url,
      method: request.method || 'GET',
      headers: request.headers,
      transformRequest : request.transformRequest,
      data: request.data
    };

    return $http(obj);
  };
    self.saveAndAuth = function(url,data){
      return $http.post(url, data, {
        headers: {'Content-Type': 'application/json','Authorization': window.localStorage['access_token']}
      });
    };

    self.queryAndAuth = function(url,query){
      return $http({
        url: url,
        method: "GET",
        params: query,
        headers: {'Content-Type': 'application/json','Authorization': window.localStorage['access_token']}
      });
    };

    self.updateAndAuth = function(url,data){
      return $http.put(url,data, {
        headers: {'Content-Type': 'application/json','Authorization': window.localStorage['access_token']}
      });
    };

    self.removeAndAuth = function(url,data){
      return $http.delete(url,data, {
        headers: {'Content-Type': 'application/json','Authorization': window.localStorage['access_token']}
      });
    };

  return self;

}]).service('ComponentsServices', [function () {

  this.SelectionManager = function SelectionManager(elementSelector, btnSelector) {
    var self = this;
    self.btn = btnSelector;
    self.element = elementSelector;
    self.selectedList = [];
    self.isDeleteMode = false;
    self.isEditMode = false;
    var danger = 'alert-danger';
    var warn = 'alert-warning';
    var btnAccent = 'md-accent';
    var btnWarn = 'md-warn';

    self.clear = function clear() {
      var component = $('.' + self.element);
      component.removeClass(danger);
      component.removeClass(warn);
      self.selectedList = [];
      self.resetStatus();
    };

    self.resetStatus = function resetStatus() {
      if ($('.' + danger).length <= 0) {
        $('.' + self.btn + ' ng-md-icon').removeClass("rotate135").addClass("reset");
        $('.' + self.btn).removeClass(btnWarn).addClass(btnAccent);
        self.isDeleteMode = false;
      } else {
        self.isDeleteMode = true;
      }

      if ($('.' + warn).length <= 0) {
        self.isEditMode = false;
      } else {
        self.isEditMode = true;
      }
    };

    self.select = function (model, event) {
      var target = $(event.$$element).children();

      if (target.hasClass(danger)) {
        target.removeClass(danger);

        var x;
        for (x in self.selectedList) {
          if (self.selectedList[x]._id == model._id) {
            delete self.selectedList[x];
          }
        }
        self.resetStatus();
      } else {
        target.addClass(danger);
        self.selectedList.push(model);
        $('.' + self.btn + ' ng-md-icon').addClass("rotate135");
        $('.' + self.btn).removeClass(btnAccent).addClass(btnWarn);
        self.isDeleteMode = true;
      }

      $('.' + self.element).removeClass(warn);
    };

    self.edit = function (model, event) {
      var target = $(event.currentTarget).children();

      if (target.hasClass(warn)) {
        self.isEditMode = false;
        return target.removeClass(warn);
      } else {
        $('.' + self.element).removeClass(warn);
        target.addClass(warn);
        $('.' + self.element).removeClass(danger);
        self.isEditMode = true;
      }

      $('.' + self.element).removeClass(danger);
      self.resetStatus();

      self.selectedList = [];
    };

    self.getSelectedList = function () {
      return self.selectedList;
    };

    self.getIsDeleteMode = function () {
      return self.isDeleteMode;
    };

    self.getIsEditMode = function () {
      return self.isEditMode;
    };
  }

}]).service('UploadService', ['$timeout', '$upload', function ($timeout, $upload) {
  var self = this;

  self.uploadRightAway = false;
  self.modalUrl = '';
  self.completed = false;

  self.changeAngularVersion = function () {
    window.location.hash = self.angularVersion;
    window.location.reload(true);
  };
  self.hasUploader = function (index) {
    return self.upload[index] != null;
  };
  self.abort = function (index) {
    self.upload[index].abort();
    self.upload[index] = null;
  };

  self.angularVersion = window.location.hash.length > 1 ? window.location.hash.substring(1) : '1.4.7';
  self.onFileSelect = function ($files) {
    self.selectedFiles = [];
    self.progress = [];
    if (self.upload && self.upload.length > 0) {
      for (var i = 0; i < self.upload.length; i++) {
        if (self.upload[i] != null) {
          self.upload[i].abort();
        }
      }
    }
    self.upload = [];
    self.uploadResult = [];
    self.selectedFiles = $files;
    self.dataUrls = [];

    function setPreview(fileReader, index) {
      fileReader.onload = function (e) {
        $timeout(function () {
          self.dataUrls[index] = e.target.result;
        });
      }
    }

    for (var i = 0; i < $files.length; i++) {
      var $file = $files[i];
      if (window.FileReader && $file.type.indexOf('image') > -1) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL($files[i]);
        setPreview(fileReader, i);
      }
      self.progress[i] = -1;
      if (self.uploadRightAway) {
        self.start(i);
      }
    }
  };

  self.start = function (index, produto, url) {

    self.progress[index] = 0;
    self.upload[index] = $upload.upload({
      url: url,
      //headers: {'Authorization': 'xxx'},
      data: {
        produto_id: 8,//produto._id ,
        slideshow: false,
        principal: false
      },
      /*
       formDataAppender: function(fd, key, val) {
       if (angular.isArray(val)) {
       angular.forEach(val, function(v) {
       fd.append(key, v);
       });
       } else {
       fd.append(key, val);
       }
       },
       */
      file: self.selectedFiles[index],
      fileFormDataName: 'myFile'
    }).then(function (response) {
      console.log('response', response.data);
      console.log(produto);
      self.item = response.data;
      self.uploadResult.push(response.data.result);
      console.log(self.uploadResult);

      self.completed = true;
      return true;

    }, null, function (evt) {
      self.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
      self.completed = false;
      return false;
    });
  };

}]).service('IonicAlertService', function ($ionicPopup) {
  var self = this;

  self.showAlert = function(titulo,body,callback) {
    var alertPopup = $ionicPopup.alert({
      cssClass : 'alert-body',
      title: titulo,
      template: body
    });

    alertPopup.then(function(res) {

      if(callback){
        callback(res);
      }

    });
  };

  return self;

}).service('ApiDecostore',
  function ($ionicPopup,ApiSpinner,IonicAlertService,ApiService,$state) {

  var self = this;

  self.curtir = function (produto) {
    ApiSpinner.show();

    if(produto.Liked){
      ApiService.remove('http://lettesapi.azurewebsites.net/api/Like/' ,  produto.Id)
        .success(function () {
          produto.Liked = false;
          ApiSpinner.hide();
        }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet.');
      });

    }else{
      ApiService.request({
        url: 'http://lettesapi.azurewebsites.net/api/Like/' + produto.Id,
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        authorization: window.localStorage['access_token']
      }).success(function () {
        produto.Liked = true;
        ApiSpinner.hide();
      }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet.');
      });
    }
    return produto.Liked;
  };

    self.adicionarProduto = function (lista,scope) {
      ApiSpinner.show();
      ApiService.saveAndAuth('http://lettesapi.azurewebsites.net/api/list/item', {
        ListId: lista.Id,
        ProductId: scope.selecionado.Id
      }).success(function () {
        ApiSpinner.hide();
        scope.popupList.close();
      }).error(function () {
        ApiSpinner.hide();
        IonicAlertService.showAlert('Atenção','Verifique sua conexão com a internet!');
      });
    };

    self.cadastrarPlaylist = function (scope) {
      scope.popupList.close();
      ApiSpinner.show();
      $state.go('app.cadastrarPlaylist',{produto:scope.selecionado});
    };

});
